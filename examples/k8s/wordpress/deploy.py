import values
from palm.map import Map

from palm.k8s.declaration import attributes as __all__
from palm.k8s import deployment


# Import allowed keys for the k8s declaration scheme, eq. to:
#
# __all__ = ["apiVersion", "kind", "metadata", "spec"]


# Vars
labels = {"app": values.name, "tier": "frontend"}


# Declarations
apiVersion = "apps/v1"  # for versions before 1.9.0 use apps/v1beta2
kind = "Deployment"
metadata = {
    "name": "wordpress",
    "labels": labels,
}

# Want to:
# - try to be less nested, i.e. more flat
# - copy (inherit) from template
# - assign values vars
# - use dot-dict notation
spec = deployment.template.spec.copy()
spec.selector.matchLabels = labels
spec.selector.strategy.type = "Recreate"


# Generate YAML
if __name__ == "__main__":
    import palm
    print(palm.to_yaml(__file__))
