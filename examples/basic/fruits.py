"""
Transpiling should produce the following YAML:

---
# A list of tasty fruits
- Apple
- Orange
- Strawberry
- Mango
"""
[
    "Apple",
    "Orange",
    "Strawberry",
    "Mango"
]
