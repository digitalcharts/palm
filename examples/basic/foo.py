"""
A test example to illustrate properties of the palm transpiler.

Usage:

    palm ./examples/basic/foo.py

Output:

    syntax:
      has_and_follows: the exact declaration order
      these_yaml_keys: go before the alphabetic ids below
    lamb_song: Mary had a little lamb. Its fleece was white as snow. And everywhere that
      Mary went, the lamb was sure to go.
    FoodBazar: null
    foo: bar

"""

# The above doc-string and the comments are ignored.
#
# Function is mapped to YAML key-value entry, so that function name becomes a key,
# the result is value, which is equivalent to YAML:
#
# foo: bar
def foo():
    return 'bar'


def syntax():
    return {
        "has_and_follows": "the exact declaration order",
        "these_yaml_keys": "go before the alphabetic ids below",
    }


# A simple module var is translated by evaluating python code.
lamb_song = "Mary had a little lamb. Its fleece was white as snow."
lamb_song += " And everywhere that Mary went, the lamb was sure to go."


class FoodBazar:
    """This doc-string is ignored during transpiling."""

    # So is this comment

    fruits = ["apple", "orange"]

    def nonstatic():
        return "this method is ignored"

    @staticmethod
    def vegetables():
        return ["cabbage", "celery", "lettuce", "rhubarb", "spinach"]

    @staticmethod
    def fresh():
        """water"""

# YAML generation can be invoked in two ways:
#
#  1) via CLI: palm <path/to/foo.py>
#  2) by running this module: python <path/to/foo.py>
#
if __name__ == "__main__":
    import palm
    print(palm.to_yaml(__file__))
