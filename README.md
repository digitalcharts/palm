# palm 🌿

> "..All your YAML is python"

**P**ython to y**A**ml **L**anguage **M**apper transpiles python to YAML, one-to-one.

YAML applications are ubiquitous, e.g. in configuration, automation, DevOps and continuous delivery pipelines and other.

However if you are tired of handling ever growing amount of YAMLs, far beyond just the elegant few configuration files or templates, then you might be looking for some alternatives. One option is to rewrite certain pieces of the overblown "functionality" using a general-purpose programming language. In that case, you might be interested how this can be done using python and *palm*.

## What is palm:

- *one-to-one mapping* - each python module generates one YAML file or output
- *keep the order* - entries of the module syntax tree become YAML tree entries: var by var, function by function, class by class
- *more than numbers and strings* - library declares primitives for advanced data-type mapping such as things as quotes and multi-lines
- general purpose programming language like python generates YAML following its own "pythonic" syntax


Later means that palm-modules follow a certain convention that is assumed when module is evaluated (imported). This offers a much higher level of flexibility to support variety of static and non-static sources to parametrize generation of your YAML. For example, 

- place YAMLs next to your code and source it
- different templates like mako, jinja2 and cheetah
- parse and source go-lang templates via bindings
- inject data from external source like databases or API requests 
- pull secrets from the key-vault

All those kind of things.


## Installation 

Palm comes as a library and a CLI tool. You can test-compile your YAML locally simply by running executing a python module or via the CLI.

	pip install palm-tool

Test if the module is installed correctly with `palm -h`.

## Develop

Local symlink/install

	pip install -e .

## Better scaffolding for template engines

Template engine(s) for YAML can be hooked by parameterizing or passing variables. 

You have more options with general programming language as a starting point:

1) use native python means like multi-lines, "%"" and  format for string substitution

2) use python template engines like mako, jinja2 and cheetah templates

3) use go-template bindings

Python bindings for go text/template

 - https://pypi.org/project/go-template/
 - https://github.com/go-python/gopy

4) finally, convert templates into YAML and place YAML next to .py 
so that all of them can be jointly picked up by plain python code.

## palm for kubernetes

Unsurprisingly, palm can replace package managers without much effort.

Commands cheat-sheet: 


| *helm*              | *palm*              |
| ------------------- |:-------------------:|
| `helm list`         | `palm k8s list`     |
| `helm install`      | `palm k8s install`  |

