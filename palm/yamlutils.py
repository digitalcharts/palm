"""
Load/dump with PyYAML.

'In order to use LibYAML based parser and emitter, use the classes CParser and CEmitter' -
See https://pyyaml.org/wiki/PyYAMLDocumentation
"""
from yaml import load, dump
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper


def load_yaml(stream):
    """Define wrapper to load yaml from stream."""
    return load(stream, Loader=Loader)


def dump_yaml(data):
    """Define wrapper convert data to YAML."""
    # keep the order for generated YAML
    Dumper.ignore_aliases = lambda *args : True
    return dump(data,
                Dumper=Dumper,
                default_flow_style=False,
                sort_keys=False)
