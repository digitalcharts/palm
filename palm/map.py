"""
Implement advanced dictionary object with dots for palm.

Features:

- behave like dict but use dot access syntax instead of "[key]" (based on Box)
- implement inheritance from other dicts or maps similar to ChainMap or defaultdict
- do no convert namedtuples
- preserve order for keys like OrderedDict

Reserved attributes - non-magic methods that exist in a Box (Map) are:

    clear, copy, from_json, from_toml, from_yaml, fromkeys, get, items,
    keys, pop, popitem, setdefault, to_dict, to_json, to_toml, to_yaml,
    update, values

You have to use ['key'] access for those attributes.

Also see print(dir(Box())) or https://pypi.org/project/python-box/

"""
from box import Box


class Map(Box):
    """Advanced dictionary object for palm."""

    def __init__(self, *args, **kwargs):
        """Construct Map."""
        defaults = {
            "default_box": True,
            "default_box_attr": Map,
        }
        updated_kwargs = defaults.copy()
        updated_kwargs.update(kwargs)
        super(Map, self).__init__(*args, **updated_kwargs)
