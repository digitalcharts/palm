"""Transpile python types and structures into YAML."""
import sys
import ast
from copy import copy
from pathlib import Path
from .map import Map, Box

config = Map({
    "ignore_magic": True,
    # pick only those fields specified in __all__
    "restrict_mod_scope": True,
})


class PyclsTranspiler(object):
    """Transpile python-class."""

    def __init__(self, ast_node, id_name):
        """Construct python-class transpiler."""
        self._defnlist = list()
        self._methodlist = list()
        self._fieldlist = list()
        self._node = ast_node
        self._name = id_name

    @property
    def def_names(self):
        """Return a list of definitions-names following the declaration order."""
        return [defn["name"] for defn in self._defnlist]

    @property
    def method_names(self):
        """Return a list of method-names following the declaration order."""
        return [method["name"] for method in self._methodlist]

    @property
    def field_names(self):
        """Return a list of attribute-names following the declaration order."""
        return [field["name"] for field in self._fieldlist]

    def parse_ast(self):
        """Call this during recursive ast-walk."""
        assert hasattr(self._node, 'body')
        assert self._node.__class__.__name__ == 'ClassDef'
        for child in self._node.body:
            if child.__class__.__name__ == 'Assign' \
                    and len(child.targets) > 0 \
                    and child.targets[0].__class__.__name__ == 'Name':
                # Only basic assignments
                field = {
                    "node": child,
                    "name": str(child.targets[0].id),
                }
                self._fieldlist.append(field)
                self._defnlist.append(field)
            elif child.__class__.__name__ == 'FunctionDef':
                # print(dir(child))
                method = {
                    "node": child,
                    "name": child.name,
                }
                self._methodlist.append(method)
                self._defnlist.append(method)
            # Ignore other definitions.
        # print(self._defnlist)

    def to_dict(self, pycls):
        """
        Get value of python-class as a dict.

        where key-value pairs are:
         - each field assignment
         - each static methods evaluation

        If one needs a different approach one should instantiate class and
        assign it to the module variable.
        """
        self.parse_ast()
        data = dict()
        defns = {d["name"]: d["node"] for d in self._defnlist}
        # pyobj = pycls()
        for key in self.def_names:
            node = defns[key]
            if key in self.method_names:
                # Evaluate only static methods with no args.
                is_static = len(node.decorator_list) > 0 \
                    and any([decr.id == 'staticmethod' for decr in node.decorator_list])
                if not is_static:
                    continue
                method = getattr(pycls, key)
                value = method()
                if value is None and hasattr(method, '__doc__'):
                    # TODO: check if method does not have 'return' keyword
                    value = method.__doc__
                data[key] = value
            elif key in self.field_names:
                # Evaluate only static assignments
                value = getattr(pycls, key)
                data[key] = value
        return data


class PymodTranspiler(object):
    """Transpile python-module."""

    def __init__(self, in_place=False):
        """Construct python-module transpiler."""
        self.in_place = in_place
        self._defnlist = list()
        self._funclist = list()
        self._classlist = list()
        self._class_transpilers = dict()
        self._ignored_keys = set()
        self._allowed_keys = set()

    @property
    def func_names(self):
        """Return a list of function-names following the declaration order."""
        return [func["name"] for func in self._funclist]

    @property
    def class_names(self):
        """Return a list of class-names following the declaration order."""
        return [cls["name"] for cls in self._classlist]

    @property
    def def_names(self):
        """Return a list of definitions-names following the declaration order."""
        return [defn["name"] for defn in self._defnlist]

    def parse_ast(self, pymod_path):
        """Learn declaration order by parsing the source code of the py-module."""
        self._funclist = list()
        self._classlist = list()
        node = ast.parse(open(pymod_path).read(), filename=pymod_path,
                         mode='exec')
        # Must be a python module, not a snippet.
        assert hasattr(node, 'body')
        # Learn the fixed order of definitions only, but only
        # when traversing not deeper than the 1-level.
        for child in node.body:
            # Track first occurrence only.
            # TODO: check for duplications
            # TODO: handle list
            # print(child.__class__.__name__)
            # print(dir(child))
            # print(child._fields)
            if child.__class__.__name__ == 'Assign':
                # Save the left hand var name of the first occurrence of assignment.
                # This ignorance any consequent mutations to the left-hand.
                # TODO: how to handle multiple targets? (a, b) = [0, 1]
                if len(child.targets) > 0 \
                        and child.targets[0].__class__.__name__ == 'Name':
                    # Only basic assignments
                    defn = {
                        "node": child,
                        "name": str(child.targets[0].id),
                    }
                    self._defnlist.append(defn)
                continue
            if child.__class__.__name__ not in ('FunctionDef', 'ClassDef'):
                # Pick only transpiled definitions.
                continue
            defn = {
                "node": child,
                "name": child.name,
            }
            self._defnlist.append(defn)
            if child.__class__.__name__ == 'FunctionDef':
                funcdef = copy(defn)
                funcdef["args"] = child.args.args
                self._funclist.append(funcdef)
            if child.__class__.__name__ == 'ClassDef':
                clsdef = copy(defn)
                # clsdef["args"] = child.args.args
                self._classlist.append(clsdef)

    def get_value(self, attrname, pymod):
        """Return evaluated result."""
        is_func = attrname in self.func_names
        is_cls = attrname in self.class_names
        if is_func:
            handle = getattr(pymod, attrname)
            value = handle()
            # if value is None, then we still check for __doc__
            if value is None and hasattr(handle, '__doc__'):
                value = handle.__doc__
            return value
        elif is_cls:
            pycls = getattr(pymod, attrname)
            cls_transpiler = self._class_transpilers[attrname]
            return cls_transpiler.to_dict(pycls)
        # Other cases: left-hand assignment, etc.
        value = getattr(pymod, attrname)
        # if value is palm.map.Map, call to_dict()
        if type(value) in [Map, Box]:
            value = value.to_dict()
        return value

    def is_ignored(self, key):
        """Filter some module attributes out."""
        if config.ignore_magic:
            if key.startswith('__') and key.endswith('__'):
                return True
        if config.restrict_mod_scope:
            if key in self._ignored_keys:
                return True
            if key not in self._allowed_keys:
                return True
        return False

    def to_dict(self, pymod_path, pymod_obj=None):
        """Transpile python-module into YAML-ready data."""
        global config
        self._syspath = copy(sys.path)
        pymod_path = Path(pymod_path)
        sys.path.append(str(pymod_path.parent.resolve()))
        self.parse_ast(str(pymod_path.resolve()))
        for clsnode in self._classlist:
            self._class_transpilers[clsnode["name"]] = PyclsTranspiler(
                clsnode["node"], clsnode["name"])
        if pymod_obj is None:
            pymod_obj = __import__(pymod_path.name.replace('.py', ''))
        # init ignored keys
        if config.restrict_mod_scope and hasattr(pymod_obj, '__all__'):
            self._allowed_keys |= set(getattr(pymod_obj, '__all__'))

        # TODO: Handle list as a special case - see `examples/basic/fruits.py`
        data = dict()
        for key in self.def_names:
            if self.is_ignored(key):
                continue
            value = self.get_value(key, pymod_obj)
            data[key] = value
        # Restore syspath
        sys.path = copy(self._syspath)
        # Return python data tree, ready to be converted into YAML.
        return data
