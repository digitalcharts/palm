"""Generate YAML."""
import os
from os.path import sep as pathsep
from glob import glob
from datetime import datetime

from palm.transpile import PymodTranspiler
from palm.yamlutils import load_yaml, dump_yaml

__all__ = ['to_dict', 'to_yaml']


def _now_str():
    """Use for time stamps."""
    now = datetime.now()
    return now.strftime("%m/%d/%Y, %H:%M:%S")


def to_dict(value):
    """Translate inline value str, usually a __doc__, into python data tree."""
    # TODO: support to_dict(Path('path/to/file.yaml'))
    assert isinstance(value, str)
    return load_yaml(value)


def _gen_from_path(pymod_path):
    assert type(pymod_path) == str
    transpiler = PymodTranspiler()
    if not os.path.exists(pymod_path):
        raise IOError("Path does not exists: {}".format(pymod_path))
    if os.path.isdir(pymod_path):
        if not pymod_path.endswith(pathsep):
            pymod_path += pathsep
        # TODO check for __init__.py and transpile as pymod
        result = ''
        for subpath in glob('{}*.py'.format(pymod_path)):
            result += _gen_from_path(subpath)
            # TODO: add newline?
        return result
    elif pymod_path.endswith('.py'):
        data = transpiler.to_dict(pymod_path)
        # Each py-module generates on YAML, so we split it. We aslo extend the split
        # with a comment in the YAML header providing some description.
        mod_header = """
# Auto-genereated by palm (do not edit)
#
#  transpiled at: {timestamp}
#    source path: {mod_path}
---
        """.format(
            mod_path=pymod_path,
            timestamp=_now_str(),
        ).strip() + "\n"
        return mod_header + dump_yaml(data)
    else:
        raise IOError("Expected path to a python module or a folder with .py files")


def to_yaml(obj):
    """Generate YAML by transpiling python-module(s)."""
    # Obviously, we don't keep formatting such as empty lines and comments,
    # but we try to keep the order of declarations.
    if isinstance(obj, str):
        return _gen_from_path(obj)
    # If obj is a module or class, then it must implement __repr__ or be otherwise
    # serializable by PyYAML, e.g. be a nested python data structure: list or dict
    return dump_yaml(obj)
