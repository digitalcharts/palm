"""
Command-line interface for palm.

See:
    ../setup.py
"""
import argparse

from palm.version import __version__
from palm.gen import to_yaml


PALM = u'\U0001F334'
PROMPT = u'{palm} palm {version}'.format(palm=PALM, version=__version__)


def main():
    """Entry point for the palm CLI."""
    parser = argparse.ArgumentParser(description=PROMPT)
    parser.add_argument('--in-place', action='store_true', help='Generate YAML next to palm-modules.')
    parser.add_argument('pyfile', nargs='+', help='path or list of .py files with palm-modules')

    args = parser.parse_args()
    if args.in_place:
        # Write YAML into a new/replaces yaml file nearby.
        raise Exception("Implement me")
    else:
        # Output multiple YAMLs to stdout
        for pyfile in args.pyfile:
            print(to_yaml(pyfile))
